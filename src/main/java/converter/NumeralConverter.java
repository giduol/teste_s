package converter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class NumeralConverter {

    private static String ZERO = "0";

    public static String formataNumero(Object entrada) {
        String saida = entrada.toString();

        //Se o primeiro caracter nao for igual a zero, vai aplicar a mascara.
        if (!saida.substring(0,1).equals(ZERO)) {

            if (saida.substring(0,1).equals(".")) {
                try {
                    saida =  ZERO + saida;
                    Long numero = Long.valueOf(saida);
                    DecimalFormat df = new DecimalFormat("#0.##");
                    saida = df.format(numero);
                } catch (Exception ex) {
                    System.out.println("Falha ao formatar o numeral: " + saida + ex);
                }
            }
        }
        return saida;
    }

    public static String formataNumeroArredondando(Object entrada) {
        String saida = entrada.toString();

        try {
            if (saida.substring(0,1).equals(".")) {
                saida = ZERO + saida;
            }
            Float entradaFloat = Float.valueOf(saida);
            BigDecimal numBigDecimal = BigDecimal.valueOf(entradaFloat);
            numBigDecimal.setScale(2, RoundingMode.HALF_DOWN);
            DecimalFormat df = new DecimalFormat("#0.##");
            saida = df.format(numBigDecimal);
        } catch (Exception ex) {
            System.out.println("Falha ao formatar o numeral: " + saida + ex);
        }
        return saida;
    }

    public static BigDecimal stringToBigDecimal(Object entrada) {
        String entradaString = formataNumero(entrada);
        Float entradaLong = Float.valueOf(entradaString);
        return BigDecimal.valueOf(entradaLong);
    }

    public static BigDecimal formataNumeroArredondandoBigDecimal(Object entrada) {
        String saida = entrada.toString();

        try {
            if (saida.substring(0,1).equals(".")) {
                saida = ZERO + saida;
            }
            Double entradaFloat = Double.valueOf(saida);
            BigDecimal numBigDecimal = BigDecimal.valueOf(entradaFloat);
            numBigDecimal.setScale(2, RoundingMode.HALF_UP);

            return numBigDecimal;

        } catch (Exception ex) {
            System.out.println("Falha ao formatar o numeral: " + saida + ex);
            return BigDecimal.ZERO;
        }
    }

}
