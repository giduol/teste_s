package database.queries;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Query {

    private static final Logger LOGGER = LogManager.getLogger(Query.class);

    private Query() {
    }

    public static String getQuery(String path) {
        StringBuilder builder = new StringBuilder();

        try (Stream<String> stream = Files.lines(Paths.get(getPath(path)))) {
            stream.forEach(dados -> builder.append(dados).append(System.lineSeparator()));
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        return builder.toString();
    }

    private static URI getPath(String path) throws URISyntaxException {
        return Query.class.getClassLoader().getResource("queries/" + path).toURI();
    }
}