package database.configuration;

import database.Connection;
import database.ConnectionBuilder;

public enum DBConnectionsConfigs {

    HOMOLOG {
        public Connection getConnection() {
            return ConnectionBuilder.builder()
                    .driverClassName("oracle.jdbc.driver.OracleDriver")
                    .url("jdbc:oracle:thin:@coredbhom.hom.sicredi.net:1521/coredb")
                    .username("AUTOMACAO_INVESTIMENTOS")
                    .password("investimentos#12")
                    .build();
        }
    },
    DEVELOP {
        public Connection getConnection() {
            return ConnectionBuilder.builder()
                    .driverClassName("oracle.jdbc.driver.OracleDriver")
                    .url("jdbc:oracle:thin:@clpger121h-scan.hom.sicredi.net:1521/coredbsus")
                    .username("developer")
                    .password("developer")
                    .build();
        }
    };

    public abstract Connection getConnection();
}
