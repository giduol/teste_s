package database.configuration;
import utils.Environment;

import static database.configuration.DBConnectionsConfigs.HOMOLOG;
import static database.configuration.DBConnectionsConfigs.DEVELOP;


public class DataBase {

    private DataBase() {
        throw new IllegalStateException("Utility class");
    }

    public static DBConnectionsConfigs getConnectionConfigs(){

        DBConnectionsConfigs connections = null;
        switch (Environment.getEnv()){
            case "homolog":
                connections = HOMOLOG;
                break;
            case "develop":
                connections = DEVELOP;
                break;

            default:
                throw new IllegalArgumentException("Ambiente "+Environment.getEnv()+" não configurado.");
        }

        return connections;
    }
}
