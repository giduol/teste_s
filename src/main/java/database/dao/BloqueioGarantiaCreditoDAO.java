    package database.dao;

        import database.Connection;
        import database.DbFactory;
        import database.DbUtils;
        import database.queries.Query;

        import java.util.List;
        import java.util.Map;

    public class BloqueioGarantiaCreditoDAO {

        DbUtils db;

        public BloqueioGarantiaCreditoDAO(Connection connection) {
            this.db = DbFactory.get(connection);
        }

        public Map<String, Object> buscarConta(String agencia, String carteira) {
            return db.lerUmaLinha(
                    Query.getQuery("buscarConta.sql")
                            .replace("<agencia>", agencia)
                            .replace("<carteira>", carteira)
            );
        }

        public Map<String, Object> buscarContaComBloqueio(String agencia, String carteira) {
            return db.lerUmaLinha(
                    Query.getQuery("buscarContaComBloqueio.sql")
                            .replace("<agencia>", agencia)
                            .replace("<carteira>", carteira)
            );
        }

        public Map<String, Object> buscarContaResgateAutomatico(String agencia, String contem) {
            return db.lerUmaLinha(
                    Query.getQuery("buscarContaResgateAutomatico.sql")
                            .replace("<agencia>", agencia)
                            .replace("<contem>", contem)
            );
        }

        public List<Map<String, Object>> buscarInvestimentos(String agencia, String conta) {
            return db.lerNLinhas(
                    Query.getQuery("buscarInvestimentos.sql")
                            .replace("<agencia>", agencia)
                            .replace("<conta>", conta)

            );
        }

        public List<Map<String, Object>> buscarInvestimentosSem23(String agencia, String conta) {
            return db.lerNLinhas(
                    Query.getQuery("buscarInvestimentosSem23.sql")
                            .replace("<agencia>", agencia)
                            .replace("<conta>", conta)

            );
        }

        public List<Map<String, Object>> buscarBloqueiosTitulo (String agencia, String conta, String numeroTitulo){

            return db.lerNLinhas(
                    Query.getQuery("buscarBloqueiosTitulo.sql")
                            .replace("<agencia>", agencia)
                            .replace("<conta>", conta)
                            .replace("<numeroTitulo>", numeroTitulo)
            );
        }

        public List<Map<String, Object>> buscarOperacoes (String agencia, String conta){
            return db.lerNLinhas(
                    Query.getQuery("buscarOperacoes.sql")
                            .replace("<agencia>", agencia)
                            .replace("<conta>", conta)
            );
        }

        public Map<String, Object> buscarCredis (String agencia){
            return db.lerUmaLinha(
                    Query.getQuery("buscarCredis.sql")
                            .replace("<agencia>", agencia)
            );
        }

        public Map<String, Object> buscarTitulosParaBloqueiosSemBloqueiosAnteriores (String agencia, String
                carteira, String produto){
            return db.lerUmaLinha(
                    Query.getQuery("buscarTitulosParaBloqueiosSemBloqueiosAnteriores.sql")
                            .replace("<agencia>", agencia)
                            .replace("<carteira>", carteira)
                            .replace("<produto>", produto)
            );
        }

        public Map<String, Object> buscarTitulosParaBloqueiosSemBloqueiosAnterioresG (String agencia, String
                carteira, String produto){
            return db.lerUmaLinha(
                    Query.getQuery("buscarTitulosParaBloqueiosSemBloqueiosAnterioresG.sql")
                            .replace("<agencia>", agencia)
                            .replace("<carteira>", carteira)
                            .replace("<produto>", produto)
            );
        }

        public Map<String, Object> buscarTitulosParaBloqueiosComBloqueiosAnteriores (String agencia, String
                carteira, String produto){
            return db.lerUmaLinha(
                    Query.getQuery("buscarTitulosParaBloqueiosComBloqueiosAnteriores.sql")
                            .replace("<agencia>", agencia)
                            .replace("<carteira>", carteira)
                            .replace("<produto>", produto)
            );
        }

        public Map<String, Object> validarTitulosBloqueados (String oid){
            return db.lerUmaLinha(
                    Query.getQuery("validarTitulosBloqueados.sql")
                            .replace("<oid>", oid)
            );
        }

        public Map<String, Object> buscarTitulosParaDesbloqueios (String agencia, String produto, String carteira){
            return db.lerUmaLinha(
                    Query.getQuery("buscarTitulosParaDesbloqueios.sql")
                            .replace("<agencia>", agencia)
                            .replace("<carteira>", carteira)
                            .replace("<produto>", produto)
            );
        }

        public Map<String, Object> buscarOidOperacaoCredito (String agencia){
            return db.lerUmaLinha(
                    Query.getQuery("buscarOidOperacaoCredito.sql")
                            .replace("<agencia>", agencia)
            );
        }

        public Map<String, Object> validarDesbloqueioPorOidOperacaoCredito (String oid){
            return db.lerUmaLinha(
                    Query.getQuery("validarDesbloqueioPorOidOperacaoCredito.sql")
                            .replace("<oid>", oid)
            );
        }

        public Map<String, Object> validarMovimentoDesbloqueioPorOidOperacaoCredito (String oid){
            return db.lerUmaLinha(
                    Query.getQuery("validarMovimentoDesbloqueioPorOidOperacaoCredito.sql")
                            .replace("<oid>", oid)
            );
        }

        public Map<String, Object> buscarData(){
            return db.lerUmaLinha(
                    Query.getQuery("buscarDataSistema.sql")
            );
        }
    }
