package utils;

public class Cooperativa {

    private Cooperativa() {
        throw new IllegalStateException("Utility class");
    }

    public static String get(){

        return (null == System.getProperty("coop") ||System.getProperty("coop").equals("")) ? PropertiesReader.getValueFromConfig("coop") : System.getProperty("coop");

    }
}