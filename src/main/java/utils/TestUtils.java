package utils;

import org.decimal4j.util.DoubleRounder;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TestUtils {

    private static DecimalFormat decimal = new DecimalFormat("#.##");
    private static DecimalFormat intFormat = new DecimalFormat("#");
    private static  String virgula = ",";
    private static  String ponto = ".";

    public String converteNumero(String valor) {
        return valor.replace(virgula, ponto);
    }

    public Double arredondaNumero(Double valor) {
        valor = DoubleRounder.round(valor, 2);
        return valor;
    }

    public String sorteiaNumero(Double min, Double max) {
        Double valor = (Math.random()*((max-min)))+min;
        valor = DoubleRounder.round(valor, 2);
        return converteNumero(valor.toString());
    }

    public String sorteiaNumeroInteiro(Integer min, Integer max) {
        Integer valor = (int)(Math.random()*((max-min)+1))+min;
        return converteNumero(valor.toString());
    }

    public String sorteiaNumeroGrande(Double min, Double max) {
        Double valor = (Math.random()*((max-min)+1))+min;
        valor = DoubleRounder.round(valor, 0);
        return converteNumero(intFormat.format(valor));
    }

    public String dataAtual(){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Calendar data = Calendar.getInstance();
        if(data.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            data.add(Calendar.DATE, +1);
        }
        else if(data.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
            data.add(Calendar.DATE, +2);
        }
        return df.format(data.getTime());
    }

    public String dataPrimeiroDiaUtil() {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Calendar data = Calendar.getInstance();
        if (data.get(Calendar.DAY_OF_MONTH) == 29 || data.get(Calendar.DAY_OF_MONTH) == 30 || data.get(Calendar.DAY_OF_MONTH) == 31) {
            data.set(Calendar.DAY_OF_MONTH, 01);
            data.add(Calendar.MONTH, +1);
            if (data.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                data.add(Calendar.DATE, +1);
            } else if (data.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
                data.add(Calendar.DATE, +2);
            }
        }
        return df.format(data.getTime());
    }

    public String dataServico(){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar data = Calendar.getInstance();
        if(data.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            data.add(Calendar.DATE, +1);
        }
        else if(data.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
            data.add(Calendar.DATE, +2);
        }
        return df.format(data.getTime());
    }

    public String proximaData(){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Calendar data = Calendar.getInstance();
        data.add(Calendar.DATE, +1);

        if(data.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            data.add(Calendar.DATE, +1);
        }
        else if(data.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
            data.add(Calendar.DATE, +2);
        }
        return df.format(data.getTime());
    }

    public String dataAnterior(){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Calendar data = Calendar.getInstance();
        if(data.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
            data.add(Calendar.DATE, -3);
        }
        else if(data.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            data.add(Calendar.DATE, -2);
        }
        else {
            data.add(Calendar.DATE, -1);
        }
        return df.format(data.getTime());
    }

    public String dataLiberacao(int diasLiberacao){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar data = Calendar.getInstance();
        data.add(Calendar.DATE,+diasLiberacao);
        if(data.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            data.add(Calendar.DATE, +1);
        }
        else if(data.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
            data.add(Calendar.DATE, +2);
        }
        return df.format(data.getTime());
    }
}