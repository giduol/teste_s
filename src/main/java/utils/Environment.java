package utils;

public class Environment {

    private Environment() {
        throw new IllegalStateException("Utility class");
    }

    public static String getEnv() {

        return (null == System.getProperty("env") ||System.getProperty("env").equals("")) ? PropertiesReader.getValueFromConfig("env") : System.getProperty("environment/develop");


    }
}