package api.requestspecification;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.config.LogConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import utils.PrintStreamExtent;
import utils.Urls;

import static io.restassured.http.ContentType.JSON;


public class BloqueioGarantiaCreditoRequestSpecification {

    public RequestSpecification getRequestSpecification(){
        return new RequestSpecBuilder()
                .setConfig(
                        new RestAssuredConfig()
                                .sslConfig(
                                        new SSLConfig().relaxedHTTPSValidation()
                                )
                                .logConfig(LogConfig
                                        .logConfig()
                                        .enablePrettyPrinting(true)
                                        .enableLoggingOfRequestAndResponseIfValidationFails(LogDetail.ALL)
                                        .defaultStream(new PrintStreamExtent(System.out))
                                )
                )
                .setContentType(JSON)
                .setBaseUri(Urls.get("baseUriBloqueioGarantiaCredito"))
                .setBasePath(Urls.get("basePathBloqueioGarantiaCredito"))
                .setRelaxedHTTPSValidation()
                .setContentType(JSON)
                .log(LogDetail.ALL)
                .build();
    }

    public RequestSpecification getRequestSpecificationLiquidar(){
        return new RequestSpecBuilder()
                .setConfig(
                        new RestAssuredConfig()
                                .sslConfig(
                                        new SSLConfig().relaxedHTTPSValidation()
                                )
                                .logConfig(LogConfig
                                        .logConfig()
                                        .enablePrettyPrinting(true)
                                        .enableLoggingOfRequestAndResponseIfValidationFails(LogDetail.ALL)
                                        .defaultStream(new PrintStreamExtent(System.out))
                                )
                )
                .setContentType(JSON)
                .setBaseUri(Urls.get("baseUriBloqueioGarantiaCredito"))
                .setBasePath(Urls.get("basePathBloqueioGarantiaCreditoLiquidar"))
                .setRelaxedHTTPSValidation()
                .setContentType(JSON)
                .log(LogDetail.ALL)
                .build();
    }


    public RequestSpecification getRequestSpecificationHealthCheck(){
        return new RequestSpecBuilder()
                .setConfig(
                        new RestAssuredConfig()
                                .sslConfig(
                                        new SSLConfig().relaxedHTTPSValidation()
                                )
                )
                .setContentType(JSON)
                .setBaseUri(Urls.get("baseUriBloqueioGarantiaCredito"))
                .build();
    }

    /*public static ResponseSpecification responseSpecification() {
        return new ResponseSpecBuilder().log(LogDetail.ALL).build();
    }*/

}
