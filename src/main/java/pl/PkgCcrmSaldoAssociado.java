package pl;

import com.google.common.collect.Lists;
import database.ChamadaPL;
import database.ParametroBuilder;
import database.ParametroDB;
import database.configuration.DataBase;
import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.OracleTypes;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
public class PkgCcrmSaldoAssociado {

    private ChamadaPL chamadaPL;

    public PkgCcrmSaldoAssociado(ChamadaPL chamadaPL) {
        this.chamadaPL = chamadaPL;
    }


    public List<Map<String, Object>> fncConsultaSaldoAssocIb(String agencia, String conta, String titulo, String carteira, String strData, String credis, String posto, String usuario, String beneficiario, String novaHome) {

        String dataSistemaStr = "";

        try {
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(strData);
            Calendar dataSistema = Calendar.getInstance();
            dataSistema.setTime(date);
            dataSistemaStr = df.format(dataSistema.getTime());
        } catch (Exception ex) {
            System.out.println( "Falha ao formatar a Data: " +  ex );
        }

        List<ParametroDB> parametros = Lists.newArrayList(
                ParametroBuilder.result().nome("result").tipo(OracleTypes.CURSOR).build(),
                ParametroBuilder.entrada().nome("pi_cAgencia").tipo(OracleTypes.VARCHAR).valor(agencia).build(),
                ParametroBuilder.entrada().nome("pi_cConta").tipo(OracleTypes.VARCHAR).valor(conta).build(),
                ParametroBuilder.entrada().nome("pi_ctitulo").tipo(OracleTypes.VARCHAR).valor(titulo).build(),
                ParametroBuilder.entrada().nome("pi_ctipoproduto").tipo(OracleTypes.VARCHAR).valor("").build(),
                ParametroBuilder.entrada().nome("pi_ccodproduto").tipo(OracleTypes.VARCHAR).valor("").build(),
                ParametroBuilder.entrada().nome("pi_ccarteira").tipo(OracleTypes.VARCHAR).valor(carteira).build(),
                ParametroBuilder.entrada().nome("pi_cflgbeneficiario").tipo(OracleTypes.VARCHAR).valor(beneficiario).build(),
                ParametroBuilder.entrada().nome("pi_dsistema").tipo(OracleTypes.DATE).valor(dataSistemaStr).build(),
                ParametroBuilder.entrada().nome("pi_cidcoop").tipo(OracleTypes.VARCHAR).valor(credis).build(),
                ParametroBuilder.entrada().nome("pi_cidposto").tipo(OracleTypes.VARCHAR).valor(posto).build(),
                ParametroBuilder.entrada().nome("pi_cidusuar").tipo(OracleTypes.VARCHAR).valor(usuario).build(),
                ParametroBuilder.entrada().nome("pi_novahome").tipo(OracleTypes.VARCHAR).valor(novaHome).build()
        );

        List<Map<String, Object>> retorno = (List<Map<String, Object>>) chamadaPL
                .connection(DataBase.getConnectionConfigs().getConnection())
                .usuario("apli_app_owner")
                .pacote("pkg_ccrm_saldo_associado")
                .pl("fnc_consulta_saldo_assoc_ib")
                .parametros(parametros)
                .executaFunction().get("result");

        return retorno;
    }
}