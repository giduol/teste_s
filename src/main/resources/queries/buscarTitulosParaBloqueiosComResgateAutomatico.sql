
SELECT *
  FROM (SELECT FCONTA AS CONTA,
               FCTAAPLI AS TITULO,
               TRUNC(SYSDATE + 720) AS DATA,
               FSALDO AS SALDO
          FROM AG<agencia>.CCR<carteira>SALD
         WHERE IS_DELETED = 'N'
           AND FSALDO > 1000
           AND SUBSTR(FCTAAPLI, 1, 2) = '<produto>'
           AND REPLACE(FCONTA, '-', '') IN
               (SELECT FCCONTA
                  FROM AG<agencia>.CCRHAPLI APLI
                  JOIN APLICADOR_AUTOMATICO AUT
                    ON APLI.FNOIDAPLI = AUT.OID_FNOIDAPLI
                 WHERE AUT.TPO_RESGATE = '1')
         ORDER BY DBMS_RANDOM.VALUE)
 WHERE ROWNUM = 1
