
SELECT AGENCIA,
       CONTA,
       VENCIMENTO,
       TITULO,
       NOME,
       CODIGO,
       BLOQ_TOTAL,
       CARTEIRA,
       EXTRACT(DAY FROM SYSDATE) || '/' || EXTRACT(MONTH FROM SYSDATE) || '/' ||
       EXTRACT(YEAR FROM SYSDATE) AS DATA
  FROM (SELECT SALD.NUM_AG   AS AGENCIA,
               SALD.FCONTA   AS CONTA,
               SALD.FDTVENC  AS VENCIMENTO,
               SALD.FCTAAPLI AS TITULO,
               APLI.FDESC    AS NOME,
               APLI.FAPLIC   AS CODIGO,
               APLI.FVENCSN  AS BLOQ_TOTAL,
               'M' AS CARTEIRA
          FROM AGUNICO.CCRMSALD SALD
         INNER JOIN AG<agencia>.CCRMAPLI APLI
            ON SUBSTR(SALD.FCTAAPLI, 1, 2) = APLI.FAPLIC
         WHERE SALD.IS_DELETED = 'N'
           AND SALD.FSALDO > 0
           AND SALD.NUM_AG = '<agencia>'
           AND SALD.FCONTA = '<conta>'
           AND APLI.FPREPOS IN ('A', 'B')
           AND APLI.FPREPOS NOT IN ('C', 'D', 'E', 'G')
           AND REPLACE(SALD.FCONTA, '-', '') NOT IN
               (SELECT HAPLI.FCCONTA
                  FROM APLICADOR_AUTOMATICO AUT
                 INNER JOIN AG<agencia>.CCRHAPLI HAPLI
                    ON AUT.OID_FNOIDAPLI = HAPLI.FNOIDAPLI
                 WHERE TPO_RESGATE = 1
                   AND HAPLI.IS_DELETED = 'N')
        UNION
        SELECT SALD.NUM_AG   AS AGENCIA,
               SALD.FCONTA   AS CONTA,
               SALD.FDTVENC  AS VENCIMENTO,
               SALD.FCTAAPLI AS TITULO,
               APLI.FDESC    AS NOME,
               APLI.FAPLIC   AS CODIGO,
               APLI.FVENCSN  AS BLOQ_TOTAL,
               'G' AS CARTEIRA
          FROM AGUNICO.CCRGSALD SALD
         INNER JOIN AG<agencia>.CCRGAPLI APLI
            ON SUBSTR(SALD.FCTAAPLI, 1, 2) = APLI.FAPLIC
         WHERE SALD.IS_DELETED = 'N'
           AND SALD.FSALDO > 0
           AND SALD.NUM_AG = '<agencia>'
           AND SALD.FCONTA = '<conta>'
           AND APLI.FPREPOS IN ('A', 'B')
           AND APLI.FPREPOS NOT IN ('C', 'D', 'E', 'G')
           AND REPLACE(SALD.FCONTA, '-', '') NOT IN
               (SELECT HAPLI.FCCONTA
                  FROM APLICADOR_AUTOMATICO AUT
                 INNER JOIN AG<agencia>.CCRHAPLI HAPLI
                    ON AUT.OID_FNOIDAPLI = HAPLI.FNOIDAPLI
                 WHERE TPO_RESGATE = 1
                   AND HAPLI.IS_DELETED = 'N'))
  ORDER BY VENCIMENTO DESC, TITULO
