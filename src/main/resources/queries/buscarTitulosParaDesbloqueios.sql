
SELECT *
  FROM (SELECT OID_BLOQUEIO_GARANTIA_CREDITO AS OID,
               DAT_VENCIMENTO_OPR_CREDITO AS VENCIMENTO,
               FLG_BLOQUEIO               AS FLG,
               NUM_AGENCIA                AS AGENCIA,
               NUM_CONTA                  AS CONTA,
               NUM_CONTA_OPR_CREDITO      AS CONTA_CREDITO,
               NUM_OPR_CREDITO            AS OPR_CREDITO,
               NUM_TITULO                 AS TITULO,
               TPO_CARTEIRA               AS CARTEIRA,
               VLR_BLOQUEADO              AS VALOR_BLOQUEADO
          FROM BLOQUEIO_GARANTIA_CREDITO
         WHERE NUM_AGENCIA = '<agencia>'
           AND FLG_BLOQUEIO <> 'E'
           AND TPO_CARTEIRA = '<carteira>'
           AND SUBSTR(NUM_TITULO,1,2) = '<produto>'
         ORDER BY DBMS_RANDOM.VALUE)
 WHERE ROWNUM = 1
