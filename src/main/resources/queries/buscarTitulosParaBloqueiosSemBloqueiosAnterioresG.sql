
SELECT *
  FROM (SELECT FCONTA AS CONTA,
               FCTAAPLI AS TITULO,
               TRUNC(SYSDATE + 720) AS DATA,
               FSALDO AS SALDO
          FROM AG<agencia>.CCR<carteira>SALD
         WHERE IS_DELETED = 'N'
           AND FSALDO > 100
           AND SUBSTR(FCTAAPLI, 1, 2) = '<produto>'
           AND FCTAAPLI NOT IN
               (SELECT NUM_TITULO FROM BLOQUEIO_GARANTIA_CREDITO)
           AND REPLACE(FCONTA,'-','') IN
               (SELECT HAPLI.FCCONTA
                  FROM APLICADOR_AUTOMATICO AUT
                 INNER JOIN AG<agencia>.CCRHAPLI HAPLI
                    ON AUT.OID_FNOIDAPLI = HAPLI.FNOIDAPLI
                 WHERE TPO_RESGATE = 2
                   AND HAPLI.IS_DELETED = 'N')
         ORDER BY DBMS_RANDOM.VALUE)
 WHERE ROWNUM = 1
