
SELECT * FROM
 (SELECT BLOQ.OID_BLOQUEIO_GARANTIA_CREDITO AS OID,
         BLOQ.DAT_VENCIMENTO_OPR_CREDITO    AS VENCIMENTO,
         BLOQ.FLG_BLOQUEIO                  AS FLG,
         BLOQ.NUM_AGENCIA                   AS NUM_AGENCIA,
         BLOQ.NUM_CONTA_OPR_CREDITO         AS CONTA,
         BLOQ.NUM_OPR_CREDITO               AS NUM_OPR_CREDITO,
         BLOQ.NUM_TITULO                    AS TITULO,
         BLOQ.TPO_CARTEIRA                  AS CARTEIRA,
         BLOQ.VLR_BLOQUEADO                 AS VALOR_BLOQUEADO,
         BLOQ.OID_OPERACAO_CREDITO          AS OID_OPR_CREDITO,
         MOVI.VLR_MOVIMENTO                 AS VALOR_MOVIMENTO,
         MOVI.TPO_MOVIMENTO                 AS TIPO_MOVIMENTO,
         BLOQ.NUM_CONTA_OPR_CREDITO         AS CONTA_OPR_CREDITO
    FROM BLOQUEIO_GARANTIA_CREDITO BLOQ
    JOIN BLOQUEIO_GAR_CRE_MOVIMENTO MOVI
      ON BLOQ.OID_BLOQUEIO_GARANTIA_CREDITO =
         MOVI.OID_BLOQUEIO_GARANTIA_CREDITO
   WHERE BLOQ.OID_BLOQUEIO_GARANTIA_CREDITO = '<oid>'
   ORDER BY BLOQ.OID_BLOQUEIO_GARANTIA_CREDITO DESC)
WHERE ROWNUM = 1
