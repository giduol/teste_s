
SELECT QUANT, OID
  FROM (SELECT COUNT(1) AS QUANT,
               OID_OPERACAO_CREDITO AS OID
          FROM BLOQUEIO_GARANTIA_CREDITO
         WHERE FLG_BLOQUEIO <> 'E'
           --AND NUM_AGENCIA = '<agencia>'
           AND OID_OPERACAO_CREDITO NOT IN (0,1)
         GROUP BY OID_OPERACAO_CREDITO
         ORDER BY DBMS_RANDOM.VALUE)
 WHERE ROWNUM = 1
