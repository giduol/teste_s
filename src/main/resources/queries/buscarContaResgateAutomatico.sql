
SELECT *
  FROM (SELECT FCONTA AS CONTA,
               FCTAAPLI AS TITULO,
               TRUNC(SYSDATE + 720) AS DATA,
               FSALDO AS SALDO
          FROM AG<agencia>.CCRGSALD
         WHERE IS_DELETED = 'N'
           AND FSALDO > 0
           AND REPLACE(FCONTA, '-', '') IN
               (SELECT FCCONTA
                  FROM AG<agencia>.CCRHAPLI APLI
                  JOIN APLICADOR_AUTOMATICO AUT
                    ON APLI.FNOIDAPLI = AUT.OID_FNOIDAPLI
                 WHERE AUT.TPO_RESGATE = '1'
                 AND AUT.TPO_STATUS <> 'D')
           AND FCONTA <contem> IN (SELECT FCONTA
                            FROM AG0727.CCRMSALD
                           WHERE IS_DELETED = 'N'
                             AND FSALDO > 0)
         ORDER BY DBMS_RANDOM.VALUE)
 WHERE ROWNUM = 1

