package testcases.Liquidar;

import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import testcases.BaseTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.responseSpecification;

public class DesbloquearPorOidOperacaoCreditoMaisDeUm extends BaseTest {

    @Test(groups = {"funcional"})
    public void bloquearTituloGarantiaCredito() {

        Map<String, Object> dadosParaDesbloqueio;

        ArrayList oids = new ArrayList();
        for(int i=0; i<5; i++){
            dadosParaDesbloqueio = bloqueioGarantiaCreditoDAO.buscarOidOperacaoCredito(coop);
            oids.add(dadosParaDesbloqueio.get("OID"));
        }

        JSONObject dados = new JSONObject();
        dados.put("codigoUsuario", "001");
        dados.put("listIdOprCredito", oids);
        dados.put("uaLogada", "01");

        String response =
        given().
                spec(specLiquidar).
                //contentType(ContentType.JSON).
                body(dados.toString()).
        when().
                put().
        then().
                //spec(responseSpecification).
                statusCode(200).
                extract().
                asString();

    }
}


