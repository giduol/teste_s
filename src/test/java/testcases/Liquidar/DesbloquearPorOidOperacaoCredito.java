package testcases.Liquidar;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import testcases.BaseTest;

import java.util.Arrays;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.responseSpecification;

public class DesbloquearPorOidOperacaoCredito extends BaseTest {

    @Test(groups = {"funcional"})
    public void bloquearTituloGarantiaCredito() {

        Map<String, Object> dadosParaDesbloqueio = bloqueioGarantiaCreditoDAO.buscarOidOperacaoCredito(coop);

        JSONObject dados = new JSONObject();
        dados.put("codigoUsuario", "001");
        dados.put("listIdOprCredito", Arrays.asList( dadosParaDesbloqueio.get("OID") ));
        dados.put("uaLogada", "01");

        String response =
        given().
                spec(specLiquidar).
                //contentType(ContentType.JSON).
                body(dados.toString()).
        when().
                put().
        then().
                //spec(responseSpecification).
                statusCode(200).
                extract().
                asString();

        Map<String, Object> validarDesbloqueio = bloqueioGarantiaCreditoDAO.validarDesbloqueioPorOidOperacaoCredito(dadosParaDesbloqueio.get("OID").toString());

        Assert.assertEquals("E", validarDesbloqueio.get("FLG_BLOQUEIO").toString());

        Assert.assertEquals(response, dadosParaDesbloqueio.get("QUANT").toString());

        Map<String, Object> validarMovimentoDesbloqueio = bloqueioGarantiaCreditoDAO.validarMovimentoDesbloqueioPorOidOperacaoCredito(validarDesbloqueio.get("OID_BLOQUEIO_GARANTIA_CREDITO").toString());

        Assert.assertNotEquals(0, validarMovimentoDesbloqueio.get("QUANT").toString());

    }
}


