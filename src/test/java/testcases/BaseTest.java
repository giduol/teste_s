package testcases;

import api.requestspecification.BloqueioGarantiaCreditoRequestSpecification;
import com.aventstack.extentreports.testng.listener.ExtentITestListenerClassAdapter;
import database.configuration.DataBase;
import database.dao.BloqueioGarantiaCreditoDAO;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import utils.Cooperativa;

import static io.restassured.RestAssured.enableLoggingOfRequestAndResponseIfValidationFails;
import static io.restassured.RestAssured.responseSpecification;

@Listeners(ExtentITestListenerClassAdapter.class)
public class BaseTest {

    protected RequestSpecification spec;
    protected RequestSpecification specLiquidar;
    protected RequestSpecification specHealthCheck;
    protected String coop;
    protected BloqueioGarantiaCreditoDAO bloqueioGarantiaCreditoDAO;

    @BeforeClass(groups = { "healthCheck", "contrato", "funcional"})
    public void baseSetUp() {
        enableLoggingOfRequestAndResponseIfValidationFails();

        spec = new BloqueioGarantiaCreditoRequestSpecification().getRequestSpecification();
        specLiquidar = new BloqueioGarantiaCreditoRequestSpecification().getRequestSpecificationLiquidar();
        specHealthCheck = new BloqueioGarantiaCreditoRequestSpecification().getRequestSpecificationHealthCheck();
        coop = Cooperativa.get();
        bloqueioGarantiaCreditoDAO = new BloqueioGarantiaCreditoDAO(DataBase.getConnectionConfigs().getConnection());
        //responseSpecification = new BloqueioGarantiaCreditoRequestSpecification().responseSpecification();
    }
}
