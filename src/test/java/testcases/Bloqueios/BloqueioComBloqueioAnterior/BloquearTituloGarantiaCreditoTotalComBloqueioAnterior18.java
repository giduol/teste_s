package testcases.Bloqueios.BloqueioComBloqueioAnterior;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import testcases.BaseTest;

import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.responseSpecification;

public class BloquearTituloGarantiaCreditoTotalComBloqueioAnterior18 extends BaseTest {

    @Test(groups = {"funcional"})
    public void bloquearTituloGarantiaCredito() {

        String produto = "18";
        String carteira = "M";

        Map<String, Object> dadosParaBloqueio1 = bloqueioGarantiaCreditoDAO.buscarTitulosParaBloqueiosSemBloqueiosAnteriores(coop, carteira, produto);

        Float valor1 = 15f;

        JSONObject dados1 = new JSONObject();
        dados1.put("codigoUsuario", "001");
        dados1.put("conta", dadosParaBloqueio1.get("CONTA"));
        dados1.put("contaOprCredito", dadosParaBloqueio1.get("CONTA"));
        dados1.put("coop", coop);
        dados1.put("datVencimentoOprCredito", dadosParaBloqueio1.get("DATA").toString().substring(0,10));
        dados1.put("numeroOprCredito", "B66");
        dados1.put("oidOperacaoCredito", 1);
        dados1.put("tipoCarteira", carteira);
        dados1.put("titulo", dadosParaBloqueio1.get("TITULO"));
        dados1.put("uaLogada", "01");
        dados1.put("valor", valor1);

        JsonPath response1 =
                given()
                        .spec(spec)
                        .contentType(ContentType.JSON)
                        .body(dados1.toString())
                .when()
                        .post()
                .then()
                        //.spec(responseSpecification)
                        .statusCode(201)
                        .extract()
                        .body()
                        .jsonPath();

        Map<String, Object> dadosParaBloqueio = bloqueioGarantiaCreditoDAO.buscarTitulosParaBloqueiosComBloqueiosAnteriores(coop, carteira, produto);

        Float valor = 100000f;

        JSONObject dados = new JSONObject();
        dados.put("codigoUsuario", "001");
        dados.put("conta", dadosParaBloqueio.get("CONTA"));
        dados.put("contaOprCredito", dadosParaBloqueio.get("CONTA_OPR_CREDITO"));
        dados.put("coop", coop);
        dados.put("datVencimentoOprCredito", dadosParaBloqueio.get("DATA").toString().substring(0,10));
        dados.put("numeroOprCredito", "B66");
        dados.put("oidOperacaoCredito", dadosParaBloqueio.get("OPERACAO_CREDITO").toString());
        dados.put("tipoCarteira", carteira);
        dados.put("titulo", dadosParaBloqueio.get("TITULO"));
        dados.put("uaLogada", "01");
        dados.put("valor", valor);

        JsonPath response =
             given()
                .spec(spec)
                .contentType(ContentType.JSON)
                .body(dados.toString())
            .when()
                .post()
            .then()
                //.spec(responseSpecification)
                .statusCode(200)
                .extract()
                .body()
                .jsonPath();

        Map<String, Object> tabelas = bloqueioGarantiaCreditoDAO.validarTitulosBloqueados(response.getString("oidBloqueioGarantiaCredito"));

        String inf = String.valueOf(response.getString("numConta"));
        Assert.assertEquals(inf, dadosParaBloqueio.get("CONTA"));
        Assert.assertEquals(tabelas.get("CONTA").toString(), dadosParaBloqueio.get("CONTA"));

        inf = String.valueOf(response.getString("numContaOprCredito"));
        Assert.assertEquals(inf, tabelas.get("CONTA_OPR_CREDITO"));

        inf = String.valueOf(response.getString("numAgencia"));
        Assert.assertEquals(inf, coop);
        Assert.assertEquals(inf, tabelas.get("NUM_AGENCIA"));

        inf = String.valueOf(response.getString("datVencimentoOprCredito"));
        Assert.assertEquals(inf, tabelas.get("VENCIMENTO").toString().substring(0,10));

        inf = String.valueOf(response.getString("numTitulo"));
        Assert.assertEquals(inf, dadosParaBloqueio.get("TITULO"));
        Assert.assertEquals(inf, tabelas.get("TITULO"));

        inf = String.valueOf(response.getString("tpoCarteira"));
        Assert.assertEquals(inf, carteira);
        Assert.assertEquals(inf, tabelas.get("CARTEIRA").toString());

        inf = String.valueOf(response.getString("oidOperacaoCredito"));
        Assert.assertEquals(inf, dadosParaBloqueio.get("OPERACAO_CREDITO").toString());
        Assert.assertEquals(inf, tabelas.get("OID_OPR_CREDITO").toString());

        inf = String.valueOf(response.getString("flgBloqueio"));
        Assert.assertEquals(inf, "T");
        Assert.assertEquals(inf, tabelas.get("FLG").toString());

        inf = String.valueOf(response.getString("numOprCredito"));
        Assert.assertEquals(inf, "B66");
        Assert.assertEquals(inf, tabelas.get("NUM_OPR_CREDITO").toString());

        inf = String.valueOf(response.getString("oidBloqueioGarantiaCredito"));
        Assert.assertEquals(inf, tabelas.get("OID").toString());
    }
}


