package testcases.Desbloqueios;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import testcases.BaseTest;

import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.responseSpecification;
import static org.hamcrest.CoreMatchers.equalTo;

public class DesbloquearTituloGarantiaCredito18 extends BaseTest {

    @Test(groups = {"funcional"})
    public void bloquearTituloGarantiaCredito() {

        String produto = "18";
        String carteira = "M";
        Float valor = 10f;

        Map<String, Object> dadosParaDesbloqueio = bloqueioGarantiaCreditoDAO.buscarTitulosParaDesbloqueios(coop, produto, carteira);

        JSONObject dados = new JSONObject();
        dados.put("codigoUsuario", "001");
        dados.put("oidBloqueioGarantiaCredito", dadosParaDesbloqueio.get("OID"));
        dados.put("uaLogada", "01");
        dados.put("valor", valor);

        JsonPath response =
        given().
                spec(spec).
                contentType(ContentType.JSON).
                body(dados.toString()).
        when().
                put().
        then().
                //spec(responseSpecification).
                statusCode(200).
                extract().
                body().
                jsonPath();

        String inf = String.valueOf(response.getString("numConta"));
        Assert.assertEquals(inf, dadosParaDesbloqueio.get("CONTA"));

        inf = String.valueOf(response.getString("numContaOprCredito"));
        Assert.assertEquals(inf, dadosParaDesbloqueio.get("CONTA_CREDITO"));

        inf = String.valueOf(response.getString("numAgencia"));
        Assert.assertEquals(inf, coop);

        inf = String.valueOf(response.getString("datVencimentoOprCredito"));
        Assert.assertEquals(inf, dadosParaDesbloqueio.get("VENCIMENTO").toString().substring(0,10));

        inf = String.valueOf(response.getString("numTitulo"));
        Assert.assertEquals(inf, dadosParaDesbloqueio.get("TITULO"));

        inf = String.valueOf(response.getString("tpoCarteira"));
        Assert.assertEquals(inf, carteira);

        inf = String.valueOf(response.getString("flgBloqueio"));
        //Assert.assertEquals(inf, "P");

        Float valorServico = Float.valueOf(response.getString("vlrBloqueado"));
        //Assert.assertEquals(valorServico, Float.valueOf(dadosParaDesbloqueio.get("VALOR_BLOQUEADO").toString())-valor);

        inf = String.valueOf(response.getString("oidBloqueioGarantiaCredito"));
        Assert.assertEquals(inf, dadosParaDesbloqueio.get("OID").toString());

    }
}


