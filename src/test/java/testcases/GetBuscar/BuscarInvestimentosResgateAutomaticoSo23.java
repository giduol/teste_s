  package testcases.GetBuscar;

  import org.testng.annotations.Test;
  import testcases.BaseTest;

  import java.util.Map;

  import static io.restassured.RestAssured.given;
  import static io.restassured.RestAssured.responseSpecification;


  public class BuscarInvestimentosResgateAutomaticoSo23 extends BaseTest {

      @Test(groups = {"funcional"})
      public void BuscarInvestimentosResgateAutomaticoSo23() {

          String agencia = coop;
          String contem = "NOT";

          Map<String, Object> associado = bloqueioGarantiaCreditoDAO.buscarContaResgateAutomatico(agencia, contem);

          String conta = associado.get("CONTA").toString();

          given().
                  spec(spec).
                  pathParam("numeroAgencia", agencia).
                  pathParam("numeroConta", conta).
                  when().
                  get("/{numeroAgencia}/{numeroConta}").
                  then().
                  //spec(responseSpecification).
                  statusCode(404);
          }  }
