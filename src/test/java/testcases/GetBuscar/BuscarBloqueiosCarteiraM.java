     package testcases.GetBuscar;

     import database.ChamadaPL;
     import io.restassured.path.json.JsonPath;
     import org.testng.Assert;
     import org.testng.annotations.Test;
     import pl.PkgCcrmSaldoAssociado;
     import testcases.BaseTest;

     import java.util.List;
     import java.util.Map;

     import static converter.NumeralConverter.formataNumero;
     import static io.restassured.RestAssured.given;
     import static io.restassured.RestAssured.responseSpecification;


     public class BuscarBloqueiosCarteiraM extends BaseTest {

         @Test(groups = {"funcional"})
         public void BuscarBloqueiosCarteiraM() {

             String agencia = coop;

             Float total = Float.valueOf(0);
             Float delta = 1f;
             String carteira = "M";

             String novaHome = "";

             Map<String, Object> associado = bloqueioGarantiaCreditoDAO.buscarContaComBloqueio(agencia, carteira);

             String conta = associado.get("CONTA").toString();

             JsonPath response =
                     given().
                             spec(spec).
                             pathParam("numeroAgencia", agencia).
                             pathParam("numeroConta", conta).
                             when().
                             get("/{numeroAgencia}/{numeroConta}").
                             then().
                             //spec(responseSpecification).
                             statusCode(200)
                             .extract()
                             .body()
                             .jsonPath();

             List<Map<String, Object>> investimentos = bloqueioGarantiaCreditoDAO.buscarInvestimentos(agencia, conta);

             for (int contarTitulo = 0; contarTitulo < investimentos.size(); contarTitulo++) {

                 Map<String, Object> buscarCredis = bloqueioGarantiaCreditoDAO.buscarCredis(agencia);
                 Map<String, Object> buscarData = bloqueioGarantiaCreditoDAO.buscarData();

                 String numeroTitulo = String.valueOf(response.getString("infoBloqueio[" + contarTitulo + "].numeroTitulo"));
                 String data_sis = buscarData.get("SYSDATE").toString().substring(0, 10);
                 String titulo = investimentos.get(contarTitulo).get("TITULO").toString();
                 String credis = buscarCredis.get("CREDIS").toString();


                 List<Map<String, Object>> bloqueios = bloqueioGarantiaCreditoDAO.buscarBloqueiosTitulo(agencia, conta, numeroTitulo);

                 PkgCcrmSaldoAssociado pl = new PkgCcrmSaldoAssociado(ChamadaPL.novo());
                 List<Map<String, Object>> dados = pl.fncConsultaSaldoAssocIb(agencia, conta, titulo, "", data_sis, credis, "01", "01","01", novaHome);

                 for (int contarBloqueio = 0; contarBloqueio < bloqueios.size(); contarBloqueio++) {

                     Map<String, Object> detalheBloqueio = response.get("infoBloqueio[" + contarTitulo + "].detalheBloqueio[" + contarBloqueio + "]");

                     String oidOperacao = detalheBloqueio.get("oidOperacaoCredito").toString();
                     Assert.assertEquals(oidOperacao, bloqueios.get(contarBloqueio).get("ID_OPERACAO").toString());

                     String oidBloqueio = detalheBloqueio.get("oidBloqueio").toString();
                     Assert.assertEquals(oidBloqueio, bloqueios.get(contarBloqueio).get("ID_BLOQUEIO").toString());

                     String numeroOprCredito = detalheBloqueio.get("numeroOprCredito").toString();
                     Assert.assertEquals(numeroOprCredito, bloqueios.get(contarBloqueio).get("NUM_OPERACAO").toString());

                     Float valorBloqueio = Float.valueOf(detalheBloqueio.get("valorBloqueio").toString());
                     Assert.assertEquals(valorBloqueio, Float.valueOf(formataNumero(dados.get(0).get("BLOQ_GCR"))));

                     }
                 Float valor = Float.valueOf(formataNumero(dados.get(0).get("BLOQ_GCR")));

                 total = total + valor;

                 System.out.println("valor----------------" + valor + "----------------------------");
                 }

        // List<Map<String, Object>> operacoes = bloqueioGarantiaCreditoDAO.buscarOperacoes(agencia, conta);

        // for (int contarOperacoes = 0; contarOperacoes < operacoes.size(); contarOperacoes++) {

        //     Float valor = Float.valueOf(formataNumero(operacoes.get(contarOperacoes).get("VALOR")));

        //     total = total + valor;

        //     System.out.println("valor----------------" + valor + "----------------------------");

        // }

             Float totalBloqueado = Float.valueOf(response.get("totalBloqueado").toString());

             Assert.assertEquals(totalBloqueado, total, delta);

            System.out.println("total--------------------" + total + "-----------------------");
            System.out.println("totalBloqueado-----------" + totalBloqueado + "-----------------------");
         }
     }
