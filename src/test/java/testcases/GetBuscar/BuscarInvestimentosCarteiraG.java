     package testcases.GetBuscar;

     import database.ChamadaPL;
     import org.testng.Assert;
     import org.testng.annotations.Test;
     import pl.PkgCcrmSaldoAssociado;
     import testcases.BaseTest;

     import java.util.List;
     import java.util.Map;

     import static converter.NumeralConverter.formataNumero;
     import static converter.NumeralConverter.formataNumeroArredondando;
     import static io.restassured.RestAssured.given;
     import static io.restassured.RestAssured.responseSpecification;
     import static org.hamcrest.CoreMatchers.equalTo;


     public class BuscarInvestimentosCarteiraG extends BaseTest {

         @Test (groups = {"funcional"})
         public void BuscarInvestimentosCarteiraG() {


             String agencia = coop;

             Float saldo = Float.valueOf(0);
             Float delta = 0.1f;
             String carteira = "G";

             String novaHome = "";

             Map<String, Object> associado = bloqueioGarantiaCreditoDAO.buscarConta(agencia, carteira);

             String conta = associado.get("CONTA").toString();

             List<Map<String, Object>> investimentos = bloqueioGarantiaCreditoDAO.buscarInvestimentos(agencia, conta);

             for (int i = 0; i < investimentos.size(); i++) {

                 Map<String, Object> buscarCredis = bloqueioGarantiaCreditoDAO.buscarCredis(agencia);
                 Map<String, Object> buscarData = bloqueioGarantiaCreditoDAO.buscarData();

                 String vencimento = investimentos.get(i).get("VENCIMENTO").toString().substring(0, 10);
                 String titulo = investimentos.get(i).get("TITULO").toString();
                 String nome = investimentos.get(i).get("NOME").toString();
                 String codigo = investimentos.get(i).get("CODIGO").toString();
                 String credis = buscarCredis.get("CREDIS").toString();
                 String bloq_total = investimentos.get(i).get("BLOQ_TOTAL").toString();
                 String tipo_carteira = investimentos.get(i).get("CARTEIRA").toString();
                 String data_sis = buscarData.get("SYSDATE").toString().substring(0, 10);

                 PkgCcrmSaldoAssociado pl = new PkgCcrmSaldoAssociado(ChamadaPL.novo());
                 List<Map<String, Object>> dados = pl.fncConsultaSaldoAssocIb(agencia, conta, titulo, "", data_sis, credis, "01", "01","01", novaHome);

                 Float saldoLiqui = Float.valueOf(formataNumero(dados.get(0).get("SDO_LIQUI")));
                 saldo = saldo + saldoLiqui;

                 System.out.println("----------------------------" + saldo + "----------------------------");
                 System.out.println("----------------------------" + saldoLiqui + "-----------------------");

                 given().
                         spec(spec).
                         pathParam("numeroAgencia", agencia).
                         pathParam("numeroConta", conta).
                         when().
                         get("/{numeroAgencia}/{numeroConta}").
                         then().
                         //spec(responseSpecification).
                         statusCode(200).
                         body("infoBloqueio[" + i + "].codigoProduto", equalTo(codigo)).
                         body("infoBloqueio[" + i + "].nomeProduto", equalTo(nome)).
                         body("infoBloqueio[" + i + "].tipoCarteira", equalTo(tipo_carteira)).
                         body("infoBloqueio[" + i + "].numeroTitulo", equalTo(titulo)).
                         body("infoBloqueio[" + i + "].saldoLiquido.toString()", equalTo(formataNumeroArredondando(saldoLiqui))).
                         body("infoBloqueio[" + i + "].dataVencimento", equalTo(vencimento)).
                         body("infoBloqueio[" + i + "].flagSomenteBloqTotal", equalTo(bloq_total));

             }

                   Float totalDisponivel =
                         given().
                         spec(spec).
                         pathParam("numeroAgencia", agencia).
                         pathParam("numeroConta", conta).
                         when().
                         get("/{numeroAgencia}/{numeroConta}").
                         then().
                         statusCode(200).
                         extract().path("totalDisponivel");

                         System.out.println("-----------------total disponivel " + totalDisponivel);
                         System.out.println("-----------------saldo " + saldo);

                         Assert.assertEquals(totalDisponivel, saldo, delta);
         }
     }